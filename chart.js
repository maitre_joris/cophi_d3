var dataset;
var temp;
var wanted;
var svg;
var columns;
var yScale;
var xScale;
var colorscale;
var res;
var replacement;
var barwidth;

d3.csv("./Vac_f.mat").then(function(data) {
  columns = data.columns;
  res = getData(data);
  wanted = res;
  var padding = 5;
  var chartWidth = .95*window.innerWidth;
  var chartHeight = .75*window.innerHeight;
  var cophiHeight = chartHeight-25;
  var margin = { top: 0, right: 20, bottom: 0, left: 40 };
  barwidth = (chartWidth/(columns.length))*0.3 ;
  // We want our our bars to take up the full height of the chart, so, we will apply a scaling factor to the height of every bar.
  yScale = d3.scaleLinear().domain([0,data.length]).range([margin.top, cophiHeight - margin.bottom]);
  xScale = d3.scaleLinear().domain([0,columns.length]).range([margin.left,columns.length * (chartWidth / columns.length) + padding])  
  colorscale = ["#66c2a5","#fc8d62","#8da0cb"]
  orderedObjects = []
  svg = d3
    .select('#my-chart')           // I'm starting off by selecting the container.
    .append('svg')               // Appending an SVG element to that container.
    .attr("id","svg")
    .attr('width', chartWidth)   // Setting the width of the SVG.
    .attr('height', chartHeight); // And setting the height of the SVG.

  // The next step is to create the rectangles that will make up the bars in our bar chart
  var Cols = svg.selectAll('g')
    .data(res)
    .enter()
    .append('g')
    .attr("id",d => d[0])
    .attr("class", (d,i) => "gContainer")
    .attr("position",(d,i) => i);

  var defaultCols = Cols   
    .append('g')
    .attr("id",d => d[0])
    .attr("class","drect");  
    
  var selCols = Cols.append('g')
    .attr("id",d => d[0])
    .attr("class","srect");

  const reducer = (accumulator, currentValue) => accumulator + currentValue;

  function addRects(container, direction){
    container.append('rect')
    .attr("id","w"+direction.toString())
    .attr("class",container.attr("class"))
    .attr("w",d=>"w"+(d[1][direction]))
    .attr('x', (v, i) => xScale(i).toString())
    .attr('y', d => container.classed("drect")?
      cophiHeight-yScale(d[1].slice(0,direction+1).reduce(reducer)):
      parseFloat(d3.select("g.drect#"+d[0]+" rect#w"+direction.toString()).node().attributes["y"].nodeValue))
    .attr('width', barwidth-padding)
    .attr('height', d=> yScale(d[1][direction]))
    .attr('fill', colorscale[direction])
    .attr("opacity",container.classed("drect")?.4:1)
    .on("click", (d,i)=> handleMouseClick(d,i));
  }
  for (let i = 0; i < 3; i++) {
    addRects(defaultCols,i);
    addRects(selCols,i);
  }

  Cols.append("text")
    .attr('x', (v, i)=> xScale(i))
    .attr("y", cophiHeight+10)
    .attr("dy", ".35em")
    .attr("class","colName")
    .attr("position",(d,i) => i)
    .style("font-size", 0.1*barwidth)
    .style('fill', 'black') 
    .text(d => d[0].substr(0, 3))
    .append("svg:title").text(d => d[0]);

    dataset = data;
    
    var filterMode=true;
    var filters=null;
    var oldXPos;
    var selCol = null;
    var mCol = null;

  function handleMouseClick(d,i) {
    let deselect=false;
    console.log("mouseclick")
    w = d.target.id.split('w')[1];
    if (filterMode){
      filters === null? filters={}:null;
      if (filters[i[0]] !== undefined){//reaction deja selectionné
        console.log("trigering "+i[0])
        if (filters[i[0]].has(w)){//deja selectionné?
          filters[i[0]].delete(w)//alors retirer
          deselect = true;
          if(filters[i[0]].size==0)delete filters[i[0]];//set vide? alors supprimer la clé
        }else{
          filters[i[0]].add(w);
        }
      }else{
        filters[i[0]] = new Set(w);
      }
      console.log("filtering data for reaction set:")
      for ([key, value] of Object.entries(filters)) {
        console.log("reaction:"+key)
        value.forEach(i=>console.log("on direction"+i));
      }
      tempD = null;
      for ([key, value] of Object.entries(filters)) {
        value.forEach(function(item, i) { value[i] = item.toString() });
        if (tempD == null) {
          tempD = data.filter(d => d[key] in value);
        }else{
          tempD = tempD.filter(d => d[key] in value);
          }
        }
      if (tempD!==null){
        console.log("selecting...")
        wanted = getData(tempD);
        for (let i=0;i<4;i++) {
          svg.selectAll("rect.srect#w"+i.toString())
          .data(wanted).transition().duration(1000)
          .attr("height", d=> yScale(d[1][i]))
          .attr("opacity",1);
        }
        // d3.select(d.target).attr("stroke-width",1)
        // .attr("stroke","black");
        
        //FEATURE 14
        //GUB : multiselection /deselection ... utilite de garder la feature? (a voir une fois PERSDEV-19 mis en place..)
        /*let currCont = getClassedParent(d.target);
        let pos = parseInt(d3.select(currCont).attr("position"));
        let prevCont = d3.select(".gContainer[position='"+(pos-1).toString()+"']");
        let nextCont = d3.select(".gContainer[position='"+(pos+1).toString()+"']");
        let pw = wanted.filter(f=>f[0]===prevCont.attr("id"));
        let nw = wanted.filter(f=>f[0]===nextCont.attr("id"));
        c = ['Orchid','yellow','pink']

        for(i=0;i<3;i++){
          d3.select(d.target.parentElement).append('rect')
          .attr("class","connections")
          .attr('x', parseFloat(d3.select(d.target).attr("x")))
          .attr('y', parseFloat(d3.select(d.target).attr("y"))
            +parseFloat(d3.select(d.target).attr("height"))
            -yScale(pw[0][1].slice(0,i+1).reduce(reducer)))
          .attr('width', ((barwidth-padding)*0.5))
          .attr('height', yScale(pw[0][1][i]))
          .attr('fill', colorscale[i])
          .attr("opacity",0.75)
          .attr("stroke-width",1)
          .attr("stroke","black")
          .attr('pointer-events', 'none');
          
          d3.select(d.target.parentElement).append('rect')
          .attr("class","connections")
          .attr('x', (barwidth-padding)*0.5+parseFloat(d3.select(d.target).attr("x")))
          .attr('y', parseFloat(d3.select(d.target).attr("y"))
            +parseFloat(d3.select(d.target).attr("height"))
            -yScale(nw[0][1].slice(0,i+1).reduce(reducer)))
          .attr('width', ((barwidth-padding)*0.5))
          .attr('height', yScale(nw[0][1][i]))
          .attr('fill', colorscale[i])
          // .attr("stroke-alignement","outer")
          .attr("stroke-width",1)
          .attr("stroke","black")
          .attr('pointer-events', 'none');
        }*/
        if (deselect){
          console.log("deselect")
          d3.select(d.target).style('stroke','red').style('stroke-width','0px');
          // d3.select(d.target.parentElement.parentElement).selectAll(".connections").remove()
        }
        else d3.select(d.target).style('stroke','red').style('stroke-width','1px')
      }
      else{
        console.log("unselecting...")
        temp = d
        for (let i=0;i<4;i++) svg.selectAll("rect.srect#w"+i.toString()).data(res).transition().duration(1000).attr("height", d=> yScale(d[1][i])).attr("opacity",1)
        d3.select(d.target).style("stroke-width",'0px')
        // Cols.selectAll(".connections").remove()
      }
    }else{
      console.log("clearing data selection")
      for (let i=0;i<4;i++) svg.selectAll("rect.srect#w"+i.toString()).data(res).transition().duration(1000).attr("height", d=> yScale(d[1][i])).attr("opacity",1)
      d3.select(d.target).style("stroke-width",'0px')
    }

}
  
  function getData(d){
    ens = {};
    columns.forEach(function(i){
      ens[i] = [];
      ens[i][0] = 0;
      ens[i][1] = 0;
      ens[i][2] = 0;
    })
    d.forEach(function(line) {
      columns.forEach(function(col){
            ens[col][line[col]] += 1
      })
    })
    return Object.entries(ens);
  }

  function getClassedParent(elem, classed='gContainer'){//elem should be d3 slection
    while (!d3.select(elem).classed(classed)) elem = elem.parentElement
    return elem;
  }
 
  function dragstarted(event){
    console.log("drag start");
    selCol = new Column(this);
    // d3.select(this).attr('pointer-events', 'none');
    oldXPos = event.x;
  }


 var deltaX;
  function onDrag(event) {
    console.log("on drag");
    
    deltaX = event.x - oldXPos;
    selCol.moveTo(event.x+deltaX);
    //si position souris < origine alors obtenir colonne de gauche
      if(deltaX<0){
        mCol = new Column(d3.select("text[position='"+(parseInt(selCol.orderPosition)-1).toString()+"']").node())
      }else{//sinon obtenir colonne de droite
        mCol = new Column(d3.select("text[position='"+(parseInt(selCol.orderPosition)+1).toString()+"']").node())
      }
      //deplacer de -delta (mouvement inverse)
      mCol.moveOf(-deltaX);
      if (((deltaX<0) && (selCol.xPos<mCol.xPos)) || ((deltaX>0) && (selCol.xPos>mCol.xPos))){
        selCol.switchPositionWith(mCol);
        mCol.assessPositionning();
      }
    oldXPos = event.x;
    pCol = mCol;
  }

  //BUG: [PERSDEV-17]z-index is buggy, once released, column'stext can't be reached anymore
  function dragended(){
    d3.selectAll("text").nodes().forEach(d=>new Column(d).assessPositionning())
    d3.selectAll("text").raise();
    selCol = null;
    mCol = null;
  }

class Column{
  
  constructor(t){
    this.D3Obj = d3.select(getClassedParent(t));
    this.orderPosition = parseInt(this.D3Obj.attr("position"));
    this.originalXPos = xScale(this.orderPosition);
  }

  get xPos() {
    return parseInt(this.D3Obj.select("rect").attr("x"));
  }

  switchPositionWith(c){
    let tPos = c.orderPosition;
    c.orderPosition = this.orderPosition;
    this.orderPosition = tPos;

    this.D3Obj.attr("position",this.orderPosition);
    c.D3Obj.attr('position',c.orderPosition);

    c.D3Obj.selectAll('text').attr('position',c.orderPosition);
    this.D3Obj.selectAll('text').attr('position',this.orderPosition);

    tPos = c.originalPos;
    c.originalPos = this.originalXPos;
    this.originalXPos = tPos;
  }

  updateXPosition(pos,d=null){
    if (d!= null) this.D3Obj.selectAll(" *").transition().duration(d).attr("x",pos.toString());
    else this.D3Obj.selectAll(" *").attr("x",pos.toString());
  }

  moveOf(delta){
    this.updateXPosition(this.xPos+delta);
  }  
  
  moveTo(pos, d=20){
    this.updateXPosition(pos,d);
  }

  assessPositionning(){
    let v=Math.abs(xScale(this.D3Obj.select(" rect").attr("x"))-this.orderPosition)/15;
    this.moveTo(xScale(this.orderPosition),v);
  }
}

  dragHandler = d3.drag()
  .on("start", dragstarted)
  .on("drag", onDrag)
  .on('end', dragended);
  dragHandler(d3.selectAll("text"));

});